const ExtractTextPlugin = require('extract-text-webpack-plugin')
const path = require('path')

const isProduction = process.env.NODE_ENV === 'production'

const extractOptions = {
  fallback: 'style-loader',
  use: [
    {
      loader: 'css-loader',
    }
  ]
}

// For production extract styles to a separate bundle
module.exports = {
  test: /\.css$/i,
  use: ExtractTextPlugin.extract(extractOptions)
}
