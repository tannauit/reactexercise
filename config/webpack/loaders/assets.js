const { env, publicPath } = require('../configuration.js')

module.exports = {
  test: /\.(jpeg|png|gif|svg|eot|ttf|woff|woff2)$/i,
  loader: 'url-loader?limit=100000'
}
