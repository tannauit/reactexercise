Rails.application.routes.draw do
  get 'alias/statistics'
  get 'alias/posts'
	put 'alias/posts/:id', to: 'alias#update_post'
  get 'app', to: 'main#index'

  resources :main, only: [:index] do
  end
end
