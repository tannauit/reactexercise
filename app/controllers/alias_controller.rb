class AliasController < ApplicationController
	skip_before_action :verify_authenticity_token, only: [:update_post]

	def statistics
    APIClient.statistics(params.permit(:from, :to), bearer_token) do |result, response|
			render status: result.code, json: response.body
		end
  end

	def posts
    APIClient.posts(params.permit(:offset, :limit), bearer_token) do |result, response|
			render status: result.code, json: response.body
		end
  end

	def update_post
    APIClient.update_post(params[:id], params.permit(:title, :description), bearer_token) do |result, response|
			render status: result.code, json: response.body
		end
	end

	private
	def bearer_token
		pattern = /^Bearer /
		header  = request.headers['Authorization']
		header.gsub(pattern, '') if header && header.match(pattern)
	end
end
