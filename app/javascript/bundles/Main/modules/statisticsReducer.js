// @flow

import type { Post, ResponseData } from '../types/global'
import type { Dispatch, ThunkAction, GetState } from '../types/redux'
import moment from 'moment'
import { APIUtil } from '../../../packs/utils/api_util'
import { handleShowErrorMessages } from '../modules/notifyMessagesReducers.js'

type Action =
  | { type: 'STATISTICS_LOADED', payload: Array<Post> }
  | { type: 'STATISTICS_DATE_CHANGED', payload: Date }

export function handleAfterLoadStatistics(statistics: Array<Post>): Action {
  return {
    type: 'STATISTICS_LOADED',
    payload: statistics
  }
}

export function handleLoadStatistics(): ThunkAction {
  return function(dispatch: Dispatch, getState: GetState): ThunkAction {
    const { user, statisticsDate } = getState()
		const date = moment(statisticsDate)
		const fromDate = date.format('YYYY-MM-DD')
		const toDate = date.add(1, 'days').format('YYYY-MM-DD')

    APIUtil.statistics(fromDate, toDate, user.token).then((responseData: ResponseData) => {
      if (responseData.success) {
        dispatch(handleAfterLoadStatistics(responseData.data))
      } else {
        dispatch(handleShowErrorMessages([responseData.message]))
      }
    })
  }
}

export function handleStatisticsDateChanged(date: Date): Action {
	return function(dispatch: Dispatch): ThunkAction {
		dispatch({
			type: 'STATISTICS_DATE_CHANGED',
			payload: date.toISOString()
		})
		dispatch(handleLoadStatistics())
	}
}

export function statisticsDate(state: string = '2017-11-18', action: Action): Date {
  switch (action.type) {
    case 'STATISTICS_DATE_CHANGED':
      return action.payload
    default:
      return state
  }
}

export default (state: Array<Post> = [], action: Action): Array<Post> => {
  switch (action.type) {
    case 'STATISTICS_LOADED':
      return action.payload
    default:
      return state
  }
}
