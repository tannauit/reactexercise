// @flow

import type { User, ResponseData } from '../types/global'
import type { Dispatch, ThunkAction } from '../types/redux'
import { APIUtil } from '../../../packs/utils/api_util'
import { handleShowErrorMessages } from '../modules/notifyMessagesReducers.js'
import { handleLoadStatistics } from '../modules/statisticsReducer'

type Action =
  | { type: 'LOGIN_SUCCESSED', payload: User }

export function handleAfterLogin(user: User): ThunkAction {
  return function(dispatch: Dispatch) {
    dispatch({
      type: 'LOGIN_SUCCESSED',
      payload: user
    })
    dispatch(handleLoadStatistics())
  }
}

export function handleLogout(): Action {
  return {
    type: 'LOGOUT',
    payload: null
  }
}

export function handleLogin(email: string, password: string): ThunkAction {
  return function(dispatch: Dispatch) {
    APIUtil.login(email, password).then((responseData: ResponseData) => {
      if (responseData.success) {
        dispatch(handleAfterLogin(responseData.data))
      } else {
        dispatch(handleShowErrorMessages([responseData.message]))
      }
    })
  }
}

export default (state: State = null, action: Action): User => {
  switch (action.type) {
    case 'LOGIN_SUCCESSED':
      return action.payload
    case 'LOGOUT':
      return action.payload
    default:
      return state
  }
}
