// @flow

import type { Post, ResponseData } from '../types/global'
import type { Dispatch, ThunkAction, GetState } from '../types/redux'
import { APIUtil } from '../../../packs/utils/api_util'
import { handleShowErrorMessages } from '../modules/notifyMessagesReducers.js'
import { handleMenuChanged } from '../modules/initialReducer'

type Action =
  | { type: 'POSTS_LOADED', payload: Array<Post> }
  | { type: 'POST_SELECTED', payload: Post }

export function handleAfterLoadPosts(posts: Array<Post>): Action {
  return {
    type: 'POSTS_LOADED',
    payload: posts
  }
}

export function handleLoadPosts(): ThunkAction {
  return function(dispatch: Dispatch, getState: GetState): ThunkAction {
    const { user } = getState()

    APIUtil.loadPosts(user.token).then((responseData: ResponseData) => {
      if (responseData.success) {
        dispatch(handleAfterLoadPosts(responseData.data))
      } else {
        dispatch(handleShowErrorMessages([responseData.message]))
      }
    })
  }
}
export function handleUpdatePost(id: number, title: string, description: string): Action {
  return function(dispatch: Dispatch, getState: GetState): ThunkAction {
    const { user } = getState()

    APIUtil.updatePost(id, { title: title, description: description }, user.token).then((responseData: ResponseData) => {
      if (responseData.success) {
				dispatch(handleMenuChanged('posts'))
      } else {
        dispatch(handleShowErrorMessages([responseData.message]))
      }
    })
  }
}

export function handleEditPost(post: Post): Action {
  return function(dispatch: Dispatch): ThunkAction {
		dispatch({
			type: 'POST_SELECTED',
			payload: post
		})
		dispatch(handleMenuChanged('post'))
	}
}

export function selectedPost(state: Post = null, action: Action): Post {
  switch (action.type) {
    case 'POST_SELECTED':
      return action.payload
    default:
      return state
  }
}

export default (state: Array<Post> = [], action: Action): Array<Post> => {
  switch (action.type) {
    case 'POSTS_LOADED':
      return action.payload
    default:
      return state
  }
}
