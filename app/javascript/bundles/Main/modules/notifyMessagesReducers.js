// @flow

type Action =
  | { type: 'SHOW_ERRORS', payload: Array<string> }
  | { type: 'SHOW_NOTICES', payload: Array<string> }
  | { type: 'REMOVE_ERROR_MESSAGE', payload: string }
  | { type: 'REMOVE_NOTICE_MESSAGE', payload: string }

export function handleShowErrorMessages(messages: Array<string>): Action {
  return {
    type: 'SHOW_ERRORS',
    payload: messages
  }
}

export function handleShowNoticeMessages(messages: Array<string>): Action {
  return {
    type: 'SHOW_NOTICES',
    payload: messages
  }
}

export function handleRemoveErrorMessage(message: string): Action {
  return {
    type: 'REMOVE_ERROR_MESSAGE',
    payload: message
  }
}

export function handleRemoveNoticeMessage(message: string): Action {
  return {
    type: 'REMOVE_NOTICE_MESSAGE',
    payload: message
  }
}

export function errorMessages(
  state: Array<string> = [],
  action: Action
): Array<string> {
  switch (action.type) {
    case 'SHOW_ERRORS':
      if (action.payload instanceof Array) {
        return action.payload
      }
      return state
    case 'REMOVE_ERROR_MESSAGE':
      return state.filter((item: string) => item !== action.payload)
    default:
      return state
  }
}

export function noticeMessages(
  state: Array<string> = [],
  action: Action
): Array<string> {
  switch (action.type) {
    case 'SHOW_NOTICES':
      if (action.payload instanceof Array) {
        return action.payload
      }
      return state
    case 'REMOVE_NOTICE_MESSAGE':
      return state.filter((item: string) => item !== action.payload)
    default:
      return state
  }
}
