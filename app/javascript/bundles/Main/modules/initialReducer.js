// @flow

import {
  handleShowErrorMessages,
  handleShowNoticeMessages
} from '../modules/notifyMessagesReducers.js'
import { handleLoadPosts } from '../modules/postsReducer'

type Action =
  | { type: 'MENU_CHANGED', payload: string }

export function handleMenuChanged(menuName: string): ThunkAction {
  return function(dispatch: Dispatch, getState: GetState) {
    dispatch(handleShowErrorMessages([]))
    dispatch(handleShowNoticeMessages([]))
    dispatch({ type: 'MENU_CHANGED', payload: menuName })
    if (menuName === 'posts') {
      dispatch(handleLoadPosts())
    }
  }
}

const reducers = {}

reducers.activeMenu = (state: string = 'dashboard', action: Action): string => {
  switch (action.type) {
    case 'MENU_CHANGED':
      return action.payload
    default:
      return state
  }
}
export default reducers
