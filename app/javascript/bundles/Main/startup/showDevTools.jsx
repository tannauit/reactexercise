// @flow
import React from 'react';
import DevTools from '../containers/DevTools';
import { render } from 'react-dom';

/**
 * ShowDebug Popup
 * @param {store} store The redux store.
 */
export default function showDevTools(store: Object) {
  const popup = window.open(
    null,
    'Redux DevTools',
    `menubar=no,location=no,resizable=yes,scrollbars=no,status=no,
    width=400,height=800`);
  // Reload in case it already exists
  popup.location.reload();

  window.setTimeout(() => {
    popup.document.write('<div id="react-devtools-root"></div>');
    render(
      <DevTools store={store} />,
      popup.document.getElementById('react-devtools-root')
    );
  }, 10);
}
