// @flow

import React from 'react';
import { Provider } from 'react-redux';
import type { Store } from 'redux';

import configureStore from '../store/mainStore';
import MainContainer from '../containers/MainContainer';
import showDevTools from './showDevTools';

type Props = {
};

class MainApp extends React.Component<Props> {
  store: Store<{}, {}>;

  constructor(props: Props) {
    super(props);
    this.store = configureStore(props);
  }

  componentDidMount() {
    showDevTools(this.store);
  }

  render() {
    return <Provider store={this.store}>
      <MainContainer />
    </Provider>;
  }
}

export default MainApp;
