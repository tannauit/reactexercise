// @flow

import React from 'react';
import { Provider } from 'react-redux';

import configureStore from '../store/mainStore';
import MainContainer from '../containers/MainContainer';

type Props = {
};

export default class MainApp extends React.Component<Props> {
  render() {
    return <Provider store={configureStore(this.props)}>
        <MainContainer />
      </Provider>;
  }
}
