// @flow

import { createStore, applyMiddleware } from 'redux'
import mainReducer from '../reducers/mainReducer'
import thunk from 'redux-thunk'

const configureStore = function(railsProps: Object): Object {
  return createStore(mainReducer, railsProps, applyMiddleware(thunk))
}

export default configureStore
