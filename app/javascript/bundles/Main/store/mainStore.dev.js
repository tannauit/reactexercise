// @flow

import { createStore, applyMiddleware, compose } from 'redux'
import type { Store } from 'redux'
import mainReducer from '../reducers/mainReducer'
import thunk from 'redux-thunk'
import DevTools from '../containers/DevTools'

const enhancer = compose(applyMiddleware(thunk), DevTools.instrument())

const configureStore = function(railsProps: Object): Store<{}, {}> {
  const store = createStore(mainReducer, railsProps, enhancer)

  /*
  if (module.hot) {
    module.hot.accept('../reducers', () =>
      store.replaceReducer(require('../reducers'))
    );
  }
  */

  return store
}

export default configureStore
