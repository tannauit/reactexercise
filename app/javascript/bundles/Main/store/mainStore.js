export default (process.env.NODE_ENV === 'production'
  ? require('./mainStore.prod.js').default
  : require('./mainStore.dev.js').default)
