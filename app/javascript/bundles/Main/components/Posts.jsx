// @flow
import React from 'react';
import TabContainer from '../containers/TabContainer'
import styles from '../styles/main.scss'
import type { Post } from '../types/global'

type Props = {
  posts: Array<Post>,
  handleEditPost: (post: Post) => {}
};

export default class Posts extends React.Component<Props> {
  render() {
		return <div>
			<table className="table table-striped">
				<thead>
					<tr>
						<th scope="col">Title</th>
						<th scope="col">Description</th>
						<th scope="col">CreatedAt</th>
						<th scope="col">UpdatedAt</th>
						<th scope="col">Actions</th>
					</tr>
				</thead>
				<tbody>
					{this.props.posts.map((post: Post, index: number) => {
						return <tr key={index} >
							<td>
								{ post.title }
							</td>
							<td >
								{ post.description }
							</td>
							<td >
								{ post.created_at }
							</td>
							<td >
								{ post.updated_at }
							</td>
							<td>
                <a href="#" onClick={this.props.handleEditPost.bind(this, post)}>Edit</a>
							</td>
						</tr>
					})}
				</tbody>
			</table>
		</div>
	}
}
