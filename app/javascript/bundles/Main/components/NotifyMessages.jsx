// @flow
import React from 'react';
import styles from '../styles/main.scss'

type Props = {
  errorMessages: Array<string>,
  noticeMessages: Array<string>,
  handleRemoveErrorMessage: (message: string) => {},
  handleRemoveNoticeMessage: (message: string) => {}
};

export default class ErrorMessages extends React.Component<Props> {

  render() {
    return (
      <div className={styles.notifications}>
        <div className={styles.errors}>
          {this.props.errorMessages.map((message, index) => {
            if (message) {
              return <div key={index} className='alert alert-danger'>
                  <button type="button" className="close" aria-label="Close" onClick={this.props.handleRemoveErrorMessage.bind(this, message)}>
                    <span aria-hidden="true">&times;</span>
                  </button>
                  {message}
                </div>
            }
          })}
        </div>
        <div className={styles.notices}>
          {this.props.noticeMessages.map((message, index) => {
            if (message) {
              return <div key={index} className='alert alert-info'>
                  <button type="button" className="close" aria-label="Close" onClick={this.props.handleRemoveNoticeMessage.bind(this, message)}>
                    <span aria-hidden="true">&times;</span>
                  </button>
                  {message}
                </div>
            }
          })}
        </div>
      </div>
    );
  }
}
