// @flow
import React from 'react';
import TabContainer from '../containers/TabContainer'
import styles from '../styles/main.scss'
import type { User } from '../types/global'

type Props = {
  user: User
};

export default class Profile extends React.Component<Props> {
  render() {
    return (
      <div className="col-md-6">
        <form role="form">
          <h2>User Profile Page</h2>
          <br />
          <div className="form-group">
            <label htmlFor="email">Email: </label>
            <input
              type="text" defaultValue={this.props.user.email}
              className="form-control" id="email" ref="email" placeholder="Email" name="email"
              disabled
            />
          </div>
          <div className="form-group">
            <label htmlFor="displayName">Name: </label>
            <input
              type="text" defaultValue={this.props.user.name}
              className="form-control" ref="displayName" id="displayName" placeholder="Display name"
              name="displayName"
              disabled
            />
          </div>
        </form>
      </div>
    );
  }
}
