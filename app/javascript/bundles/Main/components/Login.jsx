// @flow
import React from 'react';
import styles from '../styles/main.scss'
import { FadeLoader } from 'react-spinners';

type Props = {
  handleLogin: (email, password) => {}
};

export default class Login extends React.Component<Props> {

  constructor(props) {
    super(props);
    this.state = {
      email: 'joel@gmail.com',
      password: 'A@BC123456',
      loading: false
    }

    this.handleChange = this.handleChange.bind(this);
    this.handleLogin = this.handleLogin.bind(this);
  }

  handleChange(field: string, event: Event) {
    this.setState({ ...this.state, [field]: event.target.value })
  }

  handleLogin(email: string, password: string) {
    this.setState({ ...this.state, loading: true })
    this.props.handleLogin(email, password)
  }

  render() {
    return <div className="container-fluid">
      <div className="row">
        <div className="col-xs-12">
          <h3>Login</h3>
          <hr />
          <form role="form">
            <fieldset>
              <div className="form-group">
                <input
                  className="form-control"
                  placeholder="E-mail"
                  name="login"
                  type="text"
                  value={this.state.email}
                  onChange={this.handleChange}
                />
              </div>
              <div className="form-group">
                <input className="form-control"
                  placeholder="Password"
                  name="password"
                  type="password"
                  value={this.state.password}
                  onChange={this.handleChange}
                />
              </div>
              <button type="button" className="btn btn-lg btn-success btn-block" onClick={this.handleLogin.bind(this, this.state.email, this.state.password)}>Login</button>
            </fieldset>
          </form>
          <div className={styles.colCentered}>
            <FadeLoader
              color={'#123abc'}
              loading={this.state.loading}
            />
          </div>
        </div>
      </div>
    </div>;
  }
}
