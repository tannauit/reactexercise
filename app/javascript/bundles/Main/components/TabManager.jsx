
// @flow
import React from 'react';
import type { User } from '../types/global'
import HomeContainer from '../containers/HomeContainer'
import LoginContainer from '../containers/LoginContainer'
import ProfileContainer from '../containers/ProfileContainer'
import PostsContainer from '../containers/PostsContainer'
import PostContainer from '../containers/PostContainer'

type Props = {
  activeMenu: string,
  user: User
};

export default class TabManager extends React.Component<Props> {
  render() {
    return <div className="container">
      {this.contentElement()}
    </div>
  }

  contentElement() {
    if (this.props.user == null) {
      return <LoginContainer />
    }

    if (this.props.activeMenu === 'dashboard') {
      return <HomeContainer />
    }
    if (this.props.activeMenu === 'profile') {
      return <ProfileContainer />
    }
    if (this.props.activeMenu === 'posts') {
      return <PostsContainer />
    }
    if (this.props.activeMenu === 'post') {
      return <PostContainer />
    }
  }
}
