// @flow
import React from 'react';
import type { User } from '../types/global'

type Props = {
  activeMenu: string,
  user: User,
  handleMenuChanged: (menuName: string) => {},
  handleLogout: () => {}
};

export default class Tab extends React.Component<Props> {
  render() {
    if (this.props.user == null) {
      return this.beforeLoginContent()
    }
    return this.afterLoginContent()
  }

  afterLoginContent() {
    return (
     <nav className="navbar navbar-default">
      <div className="container-fluid">
        <div className="navbar-header">
          <a className="navbar-brand" href="#">ReactExercise</a>
        </div>

        <div className="collapse navbar-collapse">
          <ul className="nav navbar-nav">
            <li className={this.props.activeMenu == 'dashboard' ? 'active': ''}><a href="#" onClick={this.props.handleMenuChanged.bind(this, 'dashboard')}>Dashboard</a></li>
            <li className={this.props.activeMenu == 'posts' ? 'active': ''}><a href="#" onClick={this.props.handleMenuChanged.bind(this, 'posts')}>Posts</a></li>
          </ul>
          <ul className="nav navbar-nav navbar-right">
              <li className="dropdown">
              <a
                href="#"
                className="dropdown-toggle"
                data-toggle="dropdown"
                role="button"
                aria-haspopup="true"
                aria-expanded="false">
                {this.props.user.name}
                <span className="caret"></span>
              </a>
              <ul className="dropdown-menu">
                <li><a href="#" onClick={this.props.handleMenuChanged.bind(this, 'profile')}>Profile</a></li>
                <li role="separator" className="divider"></li>
                <li><a href="#" onClick={this.props.handleLogout.bind(this)}>Logout</a></li>
              </ul>
              </li>
            </ul>
        </div>
      </div>
    </nav>
    );
  }

  beforeLoginContent() {
    return (
      <div></div>
    );
  }
}
