// @flow
import React from 'react';
import TabContainer from '../containers/TabContainer'
import DatePicker from 'react-datepicker';
import moment from 'moment'
import ChartJS from 'react-chartjs'
import type { Post } from '../types/global'
import { FadeLoader } from 'react-spinners';
import styles from '../styles/main.scss'

type Props = {
  statistics: Array<Post>,
	statisticsDate: Date,
	handleStatisticsDateChanged: (date: Date) => {}
};

export default class Home extends React.Component<Props> {

  render() {
    if (this.props.statistics.length == 0) {
			return <div>
				<div className="row">
					<DatePicker selected={moment(this.props.statisticsDate)} onChange={this.props.handleStatisticsDateChanged.bind(this)}/>
				</div>
				<div className={styles.colCentered} >
					<FadeLoader
						color={'#123abc'}
						loading={true}
					/>
				</div>
			</div>;
    }
    let datasets = this.getDataSets()
    let labels = this.getLabels()
		const chartOptions = {
			barValueSpacing: 3,
			scaleShowValues: true,
			scales: {
				yAxes: [{
					ticks: {
						beginAtZero: true
					}
				}],
				xAxes: [{
					stacked: false,
					beginAtZero: true,
					scaleLabel: {
						labelString: 'Month'
					},
					ticks: {
						stepSize: 1,
						min: 0,
						autoSkip: false
					}
				}]
			}
		}
		return <div>
			<div className="row">
				<DatePicker selected={moment(this.props.statisticsDate)} onChange={this.props.handleStatisticsDateChanged.bind(this)}/>
				<div className="col-md-12">
					<ChartJS.Bar data={{ datasets: datasets, labels: labels }} options={chartOptions} className={styles.canvas} width="600" height="250"/>
				</div>
			</div>
		</div>;
  }

  getDataSets() {
		var colors = {"Vue": "blue", "Ruby on Rails": "purple", "React": "green"}

    var name_views = this.props.statistics.reduce(function (obj, post) {
      obj[post.post_title] = obj[post.post_title] || [];
      obj[post.post_title].push(post.views);
      return obj;
    }, {});
    return Object.keys(name_views).map(function (key) {
      return { label: key, fillColor: colors[key], data: name_views[key]}
    });
  }

	getLabels() {
    return Object.keys(this.props.statistics.reduce(function (obj, post) {
      obj[post.timestamp] = 1;
      return obj;
    }, {})).map(function(timestamp) {
      var date = new Date(timestamp)
      return date.getHours() + "h-" + date.getDate() + "/" + date.getMonth()
    });
	}

}
