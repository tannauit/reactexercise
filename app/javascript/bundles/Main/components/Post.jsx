// @flow
import React from 'react';
import TabContainer from '../containers/TabContainer'
import styles from '../styles/main.scss'

type Props = {
  post: Post,
  handleUpdatePost: (id: number, name: string, description: string) => {},
  handleMenuChanged: (menuName: string) => {}
};

export default class Post extends React.Component<Props> {
	constructor (props) {
		super(props)
		this.state = {
			title: props.post.title,
			description: props.post.description
		};
		this.handleChange = this.handleChange.bind(this);
	}
	handleChange(event) {
		var key = event.target.name
		var value = event.target.value
		this.setState({
			...this.state,
			[key]: value
		})
	}
  render() {
    return <div className="container-fluid">
      <div className="row">
        <div className="col-xs-12">
          <h3>Post</h3>
          <hr />
          <form role="form">
            <fieldset>
              <div className="form-group">
                <input
                  className="form-control"
									name="title"
                  type="text"
                  value={this.state.title}
									onChange={this.handleChange}
                />
              </div>
              <div className="form-group">
                <textarea
                  className="form-control"
                  type="text"
									name="description"
                  value={this.state.description}
									onChange={this.handleChange}
                />
              </div>
              <div className="form-group">
                <input
                  className="form-control"
                  type="text"
                  value={this.props.post.created_at}
									disabled
                />
              </div>
              <div className="form-group">
                <input
                  className="form-control"
                  type="text"
                  value={this.props.post.updated_at}
									disabled
                />
              </div>
              <button type="button" className="btn btn-lg btn-success btn-block" onClick={this.props.handleUpdatePost.bind(this, this.props.post.id, this.state.title, this.state.description)}>Update</button>
              <button type="button" className="btn btn-lg btn-default btn-block" onClick={this.props.handleMenuChanged.bind(this, 'posts')}>Cancel</button>
            </fieldset>
          </form>
        </div>
      </div>
    </div>;
	}
}
