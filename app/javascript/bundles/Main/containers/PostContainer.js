// @flow

import { connect } from 'react-redux'
import PostComponent from '../components/Post.jsx'
import type { State, Post } from '../types/global'
import { handleUpdatePost } from '../modules/postsReducer'
import { handleMenuChanged } from '../modules/initialReducer'

const mapStateToProps = function(state: State): { post: Post } {
  return {
    post: state.selectedPost
  }
}

const mapFunctions = {
	handleUpdatePost,
  handleMenuChanged
}

export default connect(mapStateToProps, mapFunctions)(PostComponent)
