// @flow

import { connect } from 'react-redux'
import NotifyMessages from '../components/NotifyMessages.jsx'
import type { State } from '../types/global'
import {
  handleRemoveErrorMessage,
  handleRemoveNoticeMessage
} from '../modules/notifyMessagesReducers'

const mapStateToProps = function(state: State) {
  return {
    errorMessages: state.errorMessages,
    noticeMessages: state.noticeMessages
  }
}

const mapFunctions = {
  handleRemoveErrorMessage,
  handleRemoveNoticeMessage
}

export default connect(mapStateToProps, mapFunctions)(NotifyMessages)
