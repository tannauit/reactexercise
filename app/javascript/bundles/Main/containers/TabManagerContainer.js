// @flow

import { connect } from 'react-redux'
import TabManager from '../components/TabManager.jsx'
import type { State, User } from '../types/global'

const mapStateToProps = function(state: State): { activeMenu: string, activeContext: string, user: User } {
  return {
    user: state.user,
    activeMenu: state.activeMenu,
    activeContext: state.activeContext
  }
}

const mapFunctions = {}

export default connect(mapStateToProps, mapFunctions)(TabManager)
