// @flow

import { connect } from 'react-redux'
import Tab from '../components/Tab.jsx'
import type { State, User } from '../types/global'
import { handleMenuChanged } from '../modules/initialReducer'
import { handleLogout } from '../modules/userReducer'

const mapStateToProps = function(state: State): { activeMenu: string, user: User } {
  return {
    activeMenu: state.activeMenu,
    user: state.user
  }
}

const mapFunctions = {
  handleMenuChanged,
  handleLogout
}

export default connect(mapStateToProps, mapFunctions)(Tab)
