// @flow

import { connect } from 'react-redux'
import Profile from '../components/Profile.jsx'
import type { State, User } from '../types/global'

const mapStateToProps = function(state: State): { user: User } {
  return {
    user: state.user
  }
}

const mapFunctions = {
}

export default connect(mapStateToProps, mapFunctions)(Profile)
