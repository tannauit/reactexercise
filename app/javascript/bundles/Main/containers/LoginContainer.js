// @flow

import { connect } from 'react-redux'
import Login from '../components/Login.jsx'
import type { State } from '../types/global'
import { handleLogin } from '../modules/userReducer'

const mapStateToProps = function(state: State): {} {
  return {}
}

const mapFunctions = {
  handleLogin: handleLogin
}

export default connect(mapStateToProps, mapFunctions)(Login)
