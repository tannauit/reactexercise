// @flow

import { connect } from 'react-redux'
import Home from '../components/Home.jsx'
import type { State, Post } from '../types/global'
import { handleStatisticsDateChanged } from '../modules/statisticsReducer'

const mapStateToProps = function(state: State): { statistics: Array<Post>, statisticsDate: Date } {
  return {
    statistics: state.statistics,
    statisticsDate: state.statisticsDate
  }
}

const mapFunctions = {
  handleStatisticsDateChanged
}

export default connect(mapStateToProps, mapFunctions)(Home)
