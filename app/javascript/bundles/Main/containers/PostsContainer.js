// @flow

import { connect } from 'react-redux'
import Posts from '../components/Posts.jsx'
import type { State, Post } from '../types/global'
import { handleEditPost } from '../modules/postsReducer'

const mapStateToProps = function(state: State): { posts: Array<Post> } {
  return {
    posts: state.posts
  }
}

const mapFunctions = {
	handleEditPost
}

export default connect(mapStateToProps, mapFunctions)(Posts)
