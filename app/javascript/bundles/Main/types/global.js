// @flow

export type User = {
  id: number,
  email: string,
  name: string,
  token: string,
}

export type Post = {
  id: number,
  post_id: number,
  post_title: string,
  timestamp: Date,
  views: number
}

export type State = {
  activeMenu: string,
  activeContext: string,
  user: User,
  statistics: Array<Post>,
  statisticsDate: Date
}

export type ResponseData = {
  success: boolean,
  messages: Array<string>,
  data: any
}
