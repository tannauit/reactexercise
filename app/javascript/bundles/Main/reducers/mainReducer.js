// @flow
import { combineReducers } from 'redux'

import initialReducer from '../modules/initialReducer'
import user from '../modules/userReducer'
import statistics, { statisticsDate } from '../modules/statisticsReducer'
import posts, { selectedPost } from '../modules/postsReducer'
import {
  errorMessages,
  noticeMessages
} from '../modules/notifyMessagesReducers'

const mainReducer = combineReducers({
  ...initialReducer,
  user: user,
  statistics: statistics,
	posts: posts,
	selectedPost: selectedPost,
  statisticsDate: statisticsDate,
  errorMessages: errorMessages,
  noticeMessages: noticeMessages
})

export default mainReducer
