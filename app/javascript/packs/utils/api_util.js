import request from 'superagent'

let API_URL = "https://api-development.innogr.am/v1/interview"
let MAIN_URL = "http://localhost:3000/alias"

export const APIUtil = {
  login: (email, password) => {
    return request
      .post(`${API_URL}/login`)
      .send({email: email, password: password})
      .then(response => {
        return { success: true, data: response.body }
      })
      .catch(function(err) {
        return { success: false, message: err.message || err.response.body.message }
      })
  },
  statistics: (fromDate, toDate, token) => {
    return request
      .get(`${MAIN_URL}/statistics?from=${fromDate}&to=${toDate}`)
      .set('Authorization', `Bearer ${token}`)
      .send()
      .then(response => {
        return { success: true, data: response.body }
      })
      .catch(function(err) {
        return { success: false, message: err.message || err.response.body.message }
      })
  },
  loadPosts: (token, offset = 0, limit = 10) => {
    return request
      .get(`${MAIN_URL}/posts?offset=${offset}&limit=${limit}`)
      .set('Authorization', `Bearer ${token}`)
      .send()
      .then(response => {
        return { success: true, data: response.body }
      })
      .catch(function(err) {
        return { success: false, message: err.message || err.response.body.message }
      })
  },
  updatePost: (id, data, token) => {
    return request
      .put(`${MAIN_URL}/posts/${id}`)
      .set('Authorization', `Bearer ${token}`)
      .send(data)
      .then(response => {
        return { success: true, data: response.body }
      })
      .catch(function(err) {
        return { success: false, message: err.message || err.response.body.message }
      })
  },
}
