class APIClient
  def self.statistics(params, token, &block)
    url = "https://api-development.innogr.am/v1/interview/statistics?from=#{params[:from]}&to=#{params[:to]}"
    RestClient.try(:get, url, "Authorization" => "#Bearer #{token}") { |response, request, result, &block|
      yield result, response
    }
  end

  def self.posts(params, token, &block)
    url = "https://api-development.innogr.am/v1/interview/posts?from=#{params[:offset]}&to=#{params[:limit]}"
    RestClient.try(:get, url, "Authorization" => "#Bearer #{token}") { |response, request, result, &block|
      yield result, response
    }
  end

  def self.update_post(id, params, token, &block)
    url = "https://api-development.innogr.am/v1/interview/posts/#{id}"
    RestClient.try(:put, url, { post: params.to_hash }, "Authorization" => "#Bearer #{token}") { |response, request, result, &block|
      yield result, response
    }
  end
end
