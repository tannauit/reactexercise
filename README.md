# README

## Install 

1. install rvm by following the instruction in https://rvm.io/
2. cd to project directory, it will switch to ruby-version and gemset, please make sure you have the same version
3. run `gem install bundler`
4. run `bundle install`
5. run `yarn install`

## Run app
1. run `rails s`
2. open other terminal and run `yarn js-watch`
3. Open [http://localhost:3000/app](http://localhost:3000/app)
